﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Banas_Video10_Part2
{
    class Television : IElectronicDevice
    {
        public int Volume;


        public void PowerOff()
        {
            Log.Information("Power has been turned off");
        }

        public void PowerOn()
        {
            Log.Information("Power has been turned on");
        }

        public void VolumeDown()
        {
            if (Volume != 0)
            {
                Volume--;
                Log.Information($"The volume has been lowered to {Volume}");
            }
        }

        public void VolumeUp()
        {
            if(Volume != 50)
            {
                Volume++;
                Log.Information($"The volume has been raised to {Volume}");
            }
        }
    }
}

//Stopped at time: 9:34
