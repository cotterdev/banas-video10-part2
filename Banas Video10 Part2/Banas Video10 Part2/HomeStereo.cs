﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;


namespace Banas_Video10_Part2
{
    class HomeStereo : IElectronicDevice
    {
        public void PowerOff()
        {
            Log.Information("Home stereo turned off.");
        }

        public void PowerOn()
        {
            Log.Information("Home stereo turned on");
        }

        public void VolumeDown()
        {
            Log.Information("Home stereo volume turned down");
        }

        public void VolumeUp()
        {
            Log.Information("Home stereo turned up");
        }
    }
}
