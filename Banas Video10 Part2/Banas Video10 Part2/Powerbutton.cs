﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banas_Video10_Part2
{
    class Powerbutton : ICommand
    {
        IElectronicDevice device;

        public Powerbutton(IElectronicDevice device)
        {
            this.device = device;
        }

        public void Execute()
        {
            device.PowerOn();    
        }

        public void Undo()
        {
            device.PowerOff();
        }
    }
}
