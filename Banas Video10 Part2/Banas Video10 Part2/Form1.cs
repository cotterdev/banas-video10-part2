﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;



namespace Banas_Video10_Part2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()                                                  // Needed to install serilog.console from NuGet
                //.WriteTo.File("tobyLog.txt", rollingInterval: RollingInterval.Day)  // This is the "sink".  Req'd when creating the logger object. Serilog.File installed from NuGet
                .CreateLogger();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Information("Hello");
            //IElectronicDevice TV3 = TVRemote.GetDevice();
            //IElectronicDevice TV2 = new Television();
            Television TV = new Television();
                     
            Powerbutton powBut = new Powerbutton(TV);
            VolumeButton volBut = new VolumeButton(TV);
           
            powBut.Execute();
            powBut.Undo();
            volBut.Execute();
            volBut.Undo();



        }
    }
}
