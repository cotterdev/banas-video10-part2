﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banas_Video10_Part2
{
    interface ICommand
    {
        void Execute();
        void Undo();
        //void VolumeUp();
        //void VolumeDown();
    }
}
